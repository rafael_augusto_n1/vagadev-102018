# Avaliação Front-End N1 #

Nossa avaliação é bem simples, pois o intuito é analisar como o candidato desenvolve os itens solicitados.
Avaliamos qualidade de código, versionamento, automatizadores, preprocessadores e etc.


*OBS.: Evite bootstrap e outros similares, pois queremos avaliar o seu código na implementação dos itens.*

### O Básico a ser executado para concorrer à vaga ###
* Crie uma branch com seu nome e desenvolva nela;
* O projeto deve ser responsivo. Lembrando que no PSD só existe o layout na versão desktop. Finalizando esse item você terá terminado o nível 01 da avaliação;

* Implemente o sub menu. Tenha em mente que ele deve aparecer clicando no hamburguer, quando estiver no crop de mobile. Desenvolvendo o sub menu você terá passado do nível 02;

*OBS.: Interações e funcionalidades não sugeridas no layout serão levadas em consideraçã.*

### SUBCHEFÃO ###
* Observe que no psd existe um lightbox para ser implementado. Ele só deve ser visível nas resoluções de desktop. Desenvolvendo o lightbox você terá passado do nível 03;

### CHEFÃO para garantir a vaga: ###
* Desenvolva o quickview. Quando clicar em algum item da prateleira ele deve aparecer como no layout.

*OBS. 01: Na pasta layout existem imagens de cada etapa.*

*OBS. 02: O candidato esta livre para trabalhar com a estrutura e tecnologia que preferir, exceto bootstrap e similares.*

*OBS.: Qualquer dúvida você pode entrar em contato comigo pelo e-mail: rafael.augusto@agencian1.com.br ou skype: rafael.asb.*